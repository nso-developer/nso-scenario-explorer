#!/usr/bin/env python3
# -*- mode: python; python-indent: 4 -*-

# TODO:
# - Handle port setting i.e. environment variable
# - Make messages for transaction start stop clearer
# - During an ongoing transaction, cdk_sock is not polled.
#   Should it? Is there a need?
# - User curses to control terminal echo etc.


import argparse
import atexit
import os
import queue
import select
import selectors
import socket
import sys
import subprocess
import termios
import time
import threading

try:
    import _ncs
    import _ncs.cdb as cdb
    import _ncs.maapi as maapi
    from ncs_pyvm import NcsPyVM
    import ncs
except ImportError:
    print("Error: Couldn't find NSO Python API modules.")
    print("Make sure you have sourced the ncsrc file.")
    sys.exit(1)

####################################################
# Set terminal to noncanonical mode, turns off echo
old_settings=None

def init_terminal_noncanonical():
   global old_settings
   old_settings = termios.tcgetattr(sys.stdin)
   new_settings = termios.tcgetattr(sys.stdin)
   new_settings[3] = new_settings[3] & ~(termios.ECHO | termios.ICANON) # lflags
   new_settings[6][termios.VMIN] = 0  # cc
   new_settings[6][termios.VTIME] = 0 # cc
   termios.tcsetattr(sys.stdin, termios.TCSADRAIN, new_settings)

   # Restore terminal when process terminates.
   @atexit.register
   def term_anykey():
       global old_settings
       if old_settings:
          termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)

#
# This Queue enables the use a single poll loop, which can be blocking.
#
class PollingQueue(queue.Queue):
    def __init__(self, maxsize=0):
        super().__init__(maxsize)
        self.r, self.w = socket.socketpair()
    def get(self, block=True, timeout=None):
        item = super().get(block, timeout)
        self.r.recv(1)
        return item
    def put(self, item):
        self.w.send(b'.')
        super().put(item)
    def fileno(self):
        return self.r.fileno()


# TransactionController attacheds a two-phase transaction subscriber to a
# running ConfD netsim instance.
#
# This allows to control of an ongoing transaction towards a netsim device, by
# either automatically accept (normal) or ask for acceptance (approval). An
# optional delay may # added to simulate a slow device.
# The default behavior is 'normal'.
#
# Prototol:
#  * Message to controller (inqueue)
#   ('approval?',)              Ask for current approval mode.
#   ('delay?',)                 Ask for current delay.
#   ('stop',)                   Stop the controller.
#   ('normal',)                 Enter normal mode.
#   ('approval',)               Enter approval mode.
#   ('delay', n)                Set delay to n seconds before accepting or ask
#                               for approval.
#   ('accept',)                 Accept the ongoing transaction.
#   ('deny',)                   Reject the ongoing transaction.
#  * Message from controller (outqueue)
#   ('started',)                The controller is started.
#   ('stopped',)                The controller stopped.
#   ('transaction',)            A transaction is ongoing.
#   ('standby',)                The ongoing transaction is completed.
#   ('accept?',)                The ongoing transaction is asking for approval.
#   ('approval', 'normal')      Current setting of approval.
#                               Sent after approval/approval?.
#   ('delay', 5.0)              Current setting of delay.
#                               Sent after delay/delay?.
#   ('connected',)              Connected to the netsim ConfD.
#   ('disconnected',)           Connection with netsim ConfD lost.
#
#
class TransactionController(threading.Thread):

    def __init__(self, name, port=_ncs.CONFD_PORT, use_msg_id=False,
                 inqueue=None, outqueue=None, path='/'):
        #self.name = name
        self.port = port
        self.use_msg_id = use_msg_id
        self.inqueue = inqueue if inqueue else PollingQueue()
        self.outqueue = outqueue if outqueue else PollingQueue()
        self.paths = path if isinstance(path, list) else [path]
        if 'servicetypes' in self.paths:
            self.paths.remove('servicetypes')
            self.paths += self.get_servicetypes() or []
        self.running = False
        self.stopping = False
        self.tcount = 0
        # netsim state variables
        self.state = 'disconnected' # disconnected/standby/transaction
        self.mode = 'normal' # normal, approval
        self.delay = 0.0
        self.bypass = False
        self.timeout = None
        self.accepted = False
        self.aborted = False
        self.pollerObject = None
        self.cdb_sock = None
        super().__init__(name=name)

    def handle_subscription(self):
        try:
            (sType, _sFlags, _sPoints) = \
                        cdb.read_subscription_socket2(self.cdb_sock)
            # TODO: Check if it is possible to distinguish
            # transaction originating from NSO from set_elem in
            # this script.
            if not self.bypass and sType == cdb.SUB_PREPARE:
                self.tcount += 1
                self._set_state('transaction')
                self.accepted = False
                self.aborted = False
                if self.delay:
                    self.timeout = time.time()+self.delay
                elif self.mode == 'normal':
                    self.accepted = True
                elif self.mode == 'approval':
                    self._send(('accept?',))
            else:
                cdb.sync_subscription_socket(self.cdb_sock,
                                             cdb.DONE_PRIORITY)
        except _ncs.error.EOF:
            self.pollerObject.unregister(self.cdb_sock)
            self._set_state('disconnected')
            self.cdb_sock = None

    def connect_subscriber(self):
        try:
            # TODO: No need to create a new socket for each connection attempt?
            self.cdb_sock = socket.socket()
            cdb.connect(self.cdb_sock, type=cdb.DATA_SOCKET,
                        ip='127.0.0.1', port=self.port)
            for path in self.paths:
                subid = cdb.subscribe2(self.cdb_sock,
                                       cdb.SUB_RUNNING_TWOPHASE,
                                       0, 0, 0, path)
            cdb.subscribe_done(self.cdb_sock)
            self._set_state('standby')
            return True
        except _ncs.error.EOF:
            pass
        except _ncs.error.Error:
            pass
        return False


    def handle_inqueue(self):
        msg, *msgdata = self.inqueue.get()
        if msg == 'stop':
            self.running = False
        elif msg == 'normal':
            self.mode = 'normal'
            self._send(('approval', self.mode))
        elif msg == 'approval':
            self.mode = 'approval'
            self._send(('approval', self.mode))
        elif msg == 'delay':
            self.delay = msgdata[0]
            self._send(('delay', self.delay))
        elif msg == 'accept':
            self.accepted = True
        elif msg == 'abort':
            self.aborted = True
        elif msg == 'approval?':
            self._send(('approval', self.mode))
        elif msg == 'delay?':
            self._send(('delay', self.delay))
        elif msg == 'reset_count':
            self.tcount = 0
            self._send(('tcount', self.tcount))

    def run(self):
        self._send(('started',))
        self.running = True
        self.pollerObject = select.poll()

        self.pollerObject.register(self.inqueue, select.POLLIN)

        next_connection_attempt = 0
        # TODO: Handle exceptions from cdb_sync...
        while not self.stopping and self.running:
            ################
            # Handle events
            ################
            fdVsEvents = self.pollerObject.poll(100)
            for fdevent in fdVsEvents:
                fd, event = fdevent
                if self.cdb_sock and fd == self.cdb_sock.fileno():
                    self.handle_subscription()
                elif fd == self.inqueue.fileno():
                    self.handle_inqueue()

            ################
            # States
            ################
            if self.state == 'disconnected':
                if time.time()>next_connection_attempt:
                    if self.connect_subscriber():
                        self.pollerObject.register(self.cdb_sock, select.POLLIN)
                    else:
                        next_connection_attempt = time.time()+3

            elif self.state == 'standby':
                pass

            else: # self.state == 'transaction'
                if self.timeout and time.time()>self.timeout:
                    if self.mode == 'normal':
                        self.accepted = True
                    else:
                        self.timeout = None
                        self._send(('accept?',))

                if self.accepted:
                    cdb.sync_subscription_socket(self.cdb_sock,
                                                 cdb.DONE_PRIORITY)
                    self._set_state('standby')
                elif self.aborted:
                    cdb.sub_abort_trans(self.cdb_sock, 1, 0, 0, 'Forbidden!')
                    self._set_state('standby')


        self._send(('stopped',))
        self.cdb_sock.close()

    def put(self, msg):
        self.inqueue.put(msg)

    def _send(self, msg):
        if self.use_msg_id:
            msg = (self.name, *msg)
        self.outqueue.put(msg)

    def _set_state(self, state):
        if self.state == 'disconnected':
            # TODO: Sending a message in a function that is not dependant on
            #       the input state is not a good practice as it might discard
            #       the input states intended message.
            self._send(('connected',))
        elif state == 'transaction':
            self._send((state, self.tcount))
        else:
            self._send((state,))
        self.state = state

    def stop(self):
        self.stopping = True

    def set_elem(self, value, path):
        addr = '127.0.0.1'
        user = "admin"
        groups = ["admin"]
        dbname = _ncs.CANDIDATE
        context = "maapi"
        maapisock = socket.socket()

        try:
            maapi.connect(sock=maapisock, ip=addr, port=self.port)
            maapi.start_user_session(maapisock, user, context, groups, addr,
                                     _ncs.PROTO_TCP)
            self.bypass = True # Prevent subscriber to intercept transaction
            tid = maapi.start_trans(maapisock, dbname, _ncs.READ_WRITE)
            maapi.set_elem(maapisock, tid, _ncs.Value(value, _ncs.C_BUF), path)
            maapi.apply_trans(maapisock, tid, False)
            maapi.finish_trans(maapisock, tid)
            maapi.candidate_commit(maapisock)
            self.bypass = False # Allow subscriber to intercept transactions
            return True
        except _ncs.error.Error:
            return None
        finally:
            maapisock.close()


    def get_servicetypes(self):
        # Get available registered service types.
        # NSO 4.x, 5.x - Call Action /ncs:services/get-servicetypes provided by
        #                package service-info.
        # NSO 6.x      - Read list /ncs:services/service-type.
        try:
            with ncs.maapi.single_read_trans('admin', 'system',
                                             port=self.port) as t:
                r = ncs.maagic.get_root(t)
                version = r.ncs_state.version.split('.')
                if int(version[0]) < 6:
                    getsps = r.ncs__services.get_servicetypes
                    return [s.path for s in getsps().service]
                else:
                    return [t.name for t in r.ncs__services.service_type]
        except Exception as e:
            print(e)
            return None


def parseArgs(argv):
    parser = argparse.ArgumentParser(description='Interactively block netsim.')
    parser.add_argument('-v', '--verbose', action='store_true', help="Show verbose information")
    parser.add_argument('-d', '--delay', type=float, help="Add delay, in milliseconds")
    parser.add_argument('-c', '--count', type=int, help='Number of transactions to intercept (default: inifinite)')
    parser.add_argument('-P', '--path', type=str, nargs="*", default='/',
                        help="IPC port for the netsim instance")

    # Approval or nah
    app = parser.add_mutually_exclusive_group()
    app.add_argument('-a', '--approval', action='store_true', default=True, help='Require manual approval')
    app.add_argument('-n', '--no-approval', action='store_false', dest='approval', help='Do not require manual approval')

    # Where to find netsim
    g = parser.add_mutually_exclusive_group()
    g.add_argument('netsim', metavar='NETSIM', type=str, nargs='?',
            help='Name of the netsim instance')
    g.add_argument('-p', '--port', type=int, help="IPC port for the netsim instance")
    return parser.parse_args()


running = True
waiting_for_approval = False
mode = ""
delay = ""

def main(args):
    if args.netsim:
        try:
            outp = subprocess.check_output(['ncs-netsim', 'get-port', args.netsim])
            port = int(outp)
        except subprocess.CalledProcessError:
            print('ERROR: Could not read netsim {}'.format(args.netsim))
            sys.exit(1)
    elif args.port:
        port = args.port
    else:
        port = int(os.environ['CONFD_IPC_PORT'])


    controller = TransactionController(args.netsim, port, path=args.path)
    controller.start()

    def send_msg(msg):
        if args.verbose: print("-->", msg)
        controller.inqueue.put(msg)


    def handle_key(c):
        global running
        if waiting_for_approval:
            if c in "nN":
                print()
                send_msg(('abort',))
            elif c in "yY\n ":
                print()
                send_msg(('accept',))
        if c == 'q':
            running = False
        elif c == 'm':
            if mode == 'normal':
                send_msg(('approval',))
            else:
                send_msg(('normal',))
        elif c == 's':
            if controller.state == 'disconnected':
                print("Starting netsim.")
                subprocess.run(['ncs-netsim', 'start', args.netsim])
            else:
                print("Stopping netsim.")
                subprocess.run(['ncs-netsim', 'stop', args.netsim])
        elif c == 'h':
            if controller.set_elem(args.netsim, 'hostname'):
                print(f"hostname is set to '{args.netsim}'.")
            else:
                print("Failed to set hostname.")
        elif c == 'o':
            if controller.set_elem('out-of-sync', 'hostname'):
                print("hostname is set to 'out-of-sync'.")
            else:
                print("Failed to set hostname.")
    def print_mode():
        global mode, delay
        if mode == 'normal':
            print(f"Transactions are automatically accepted", end="")
            if delay:
                print(f" after a delay of {delay} seconds", end="")
            print(".")
        elif mode == 'approval':
            print(f"Transactions are asked for approval", end="")
            if delay:
                print(f" after a delay of {delay} seconds", end="")
            print(".")
    def handle_event(msg):
        global running, waiting_for_approval, mode, delay
        msgtype, *msgdata = msg
        if args.verbose: print("<--", msg)
        if msgtype == 'started':
            print("Controller has started.")
            if args.delay:
                send_msg(('delay', args.delay))
            if args.approval:
                send_msg(('approval',))
        elif msgtype == 'stopped':
            print("Disconnected from netsim.")
            running = False
        elif msgtype == 'connected':
            print("Connected to netsim.")
        elif msgtype == 'disconnected':
            print("Connection to netsim lost.")
        elif msgtype == 'transaction':
            print("Transaction ongoing...")
            if controller.delay:
                print(f"Delaying transaction for {controller.delay} seconds")
        elif msgtype == 'accept?':
            print("Accept (Y/n)", end='', flush=True)
            waiting_for_approval = True
        elif msgtype == 'standby':
            print("Transaction ended!")
            if args.count and c>=args.count:
                send_msg(('stop',))
        elif msgtype == 'approval':
            mode, = msgdata
            print_mode()
        elif msgtype == 'delay':
            delay, = msgdata
            print_mode()

    print("NSO Netsim Controller")
    print("""Press:
  'q'               to quit.
  'm'               to toggle approval mode.
  'y/space/return'  to accept a transaction.
  'n'               to deny a transaction.
  's'               to start/stop the netsim.
""")

    init_terminal_noncanonical()

    #########################
    # EVENT LOOP
    #########################

    try:
        c = 0
        pobj = select.poll()
        pobj.register(sys.stdin, select.POLLIN)
        pobj.register(controller.outqueue, select.POLLIN)
        while running:
            fdevents = pobj.poll(100)
            for fd, event in fdevents:
                if fd == sys.stdin.fileno():
                    c = sys.stdin.read(1)
                    handle_key(c)
                elif fd == controller.outqueue.fileno():
                    msg = controller.outqueue.get()
                    handle_event(msg)

    except KeyboardInterrupt:
        pass

    send_msg(('stop',))
    print("Terminating")
    controller.join()


if __name__ == "__main__":
    main(parseArgs(sys.argv[1:]))
