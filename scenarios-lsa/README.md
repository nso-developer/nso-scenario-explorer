# Overview

This collection of scenarions are about to get an understanding how the
commit-queue settings, atomic and error-option, affects the behavior of NSO in
an LSA setup and what the resulting state will be in terms of devices sync state
and if the service instance(s) are rolled back or not when a device is not
responding.

## Common scenario criterias

 - devices global-settings commit-queue retry-attemts != unlimited
 - stacked services

## Lessons to learn

NOTE: They are a summary of running these scenarions, but there might be
other cirumstances giving alternate results.

 - A commit-queue id is always returned from the commit command.
 - For rollback to occur atomic need to be true.
 - Rollback of the service(s) occur when all retry attemps have been consumed
   and either:
   - error-option is rollback-on-error
   - error-option is stop-on-error and rollback action is called on the locked
    commit-queue item.
 - When no rollback has occurred:
   - atomic true -> all devices will be out-of-sync.
   - atomic false -> only failed devices will be out-of-sync.
 - It is always recommended to have 'out-of-sync-commit-behaviour' set to
   'accept' on the RFS devices as they will otherwise be out-of-sync as a result
   of changes in data not visible from the upper node.
