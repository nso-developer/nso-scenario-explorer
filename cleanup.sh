#!/bin/sh

# Start netsim devices
ncs-netsim status ex0 >/dev/null 2>&1; [ $? = 1 ] && ncs-netsim start ex0
ncs-netsim status ex1 >/dev/null 2>&1; [ $? = 1 ] && ncs-netsim start ex1
ncs-netsim status ex2 >/dev/null 2>&1; [ $? = 1 ] && ncs-netsim start ex2
ncs-netsim status ex3 >/dev/null 2>&1; [ $? = 1 ] && ncs-netsim start ex3
ncs-netsim status ex4 >/dev/null 2>&1; [ $? = 1 ] && ncs-netsim start ex4
ncs-netsim status ex5 >/dev/null 2>&1; [ $? = 1 ] && ncs-netsim start ex5
ncs-netsim status ex6 >/dev/null 2>&1; [ $? = 1 ] && ncs-netsim start ex6
ncs-netsim status ex7 >/dev/null 2>&1; [ $? = 1 ] && ncs-netsim start ex7

if [ -e SINGLE-BUILD ]; then
    # Unlock queue-items
    ncs_cli -u admin -C <<EOL
      devices commit-queue clear
EOL
    ncs_cli -u admin -C <<EOL
      devices commit-queue completed purge
EOL
    # Sync devices
    ncs_cli -u admin -C <<EOL
      devices sync-from
      alarms purge-alarms alarm-status any
EOL
    # Restore config
    ncs_cli -u admin -C <<EOL
      config
        no top
        no top-nano
        no base-config
        no hostname
        no intf
        no resources-top vid
        no resources-top-nano vid
        no devices global-settings
        devices global-settings read-timeout 300
        devices global-settings write-timeout 300
        devices global-settings commit-queue retry-attempts 2
        devices global-settings commit-queue retry-timeout 5
        no devices device ex0 config r:sys interfaces interface eth0 unit
        no devices device ex1 config r:sys interfaces interface eth0 unit
        no devices template
        commit
        exit
      exit
EOL
fi
if [ -e LSA-BUILD ]; then
    # Unlock queue-items
    NCS_IPC_PORT=4570 ncs_cli -u admin -C <<EOL
      devices commit-queue queue-item * unlock
EOL
    NCS_IPC_PORT=4570 ncs_cli -u admin -C <<EOL
      devices commit-queue completed purge
EOL
    NCS_IPC_PORT=4570 ncs_cli -u admin -C <<EOL
      config
        no rfs-vlan:services vlan
        no devices global-settings
        devices global-settings read-timeout 300
        devices global-settings write-timeout 300
        devices global-settings commit-queue retry-attempts 2
        devices global-settings commit-queue retry-timeout 5
        no devices template
        commit
        exit
      exit
EOL
    # Sync devices
    NCS_IPC_PORT=4570 ncs_cli -u admin -C <<EOL
      devices sync-from
      alarms purge-alarms alarm-status any
EOL
    # Unlock queue-items
    NCS_IPC_PORT=4571 ncs_cli -u admin -C <<EOL
      devices commit-queue queue-item * unlock
EOL
    NCS_IPC_PORT=4571 ncs_cli -u admin -C <<EOL
      devices commit-queue completed purge
EOL
    NCS_IPC_PORT=4571 ncs_cli -u admin -C <<EOL
      config
        no rfs-vlan:services vlan
        no devices global-settings
        devices global-settings read-timeout 300
        devices global-settings write-timeout 300
        devices global-settings commit-queue retry-attempts 2
        devices global-settings commit-queue retry-timeout 5
        no devices template
        commit
        exit
      exit
EOL
    # Sync devices
    NCS_IPC_PORT=4571 ncs_cli -u admin -C <<EOL
      devices sync-from
      alarms purge-alarms alarm-status any
EOL
    # Unlock queue-items
    ncs_cli -u admin -C <<EOL
      devices commit-queue clear
EOL
    ncs_cli -u admin -C <<EOL
      devices commit-queue completed purge
EOL
    # Sync devices
    ncs_cli -u admin -C <<EOL
      devices sync-from
      alarms purge-alarms alarm-status any
EOL
    ncs_cli -u admin -C <<EOL
      config
        no top
        no resources-top vid
        no devices global-settings
        devices global-settings read-timeout 300
        devices global-settings write-timeout 300
        devices global-settings commit-queue retry-attempts 2
        devices global-settings commit-queue retry-timeout 5
        no devices template
        commit
        exit
      exit
EOL
fi
