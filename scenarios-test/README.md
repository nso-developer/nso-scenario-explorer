# Overview

This collection of scenarions to test individual commands and their behavior.

It was added to test such commands that may have built-in checks to test output
from a command, like 'check-sync' and 'display-list'.
They tests checks where added to be able to detect changes in behavior of NSO
when using Commit Queues.
