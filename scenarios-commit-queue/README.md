# Overview

This collection of scenarions where created by Dan Sullivan for NSO Developer
Days 2020 and adapted to be run with the new 'nso-scenario' script.

His presention can be found here:

 - [Video presentation](https://www.youtube.com/watch?v=9pEdvZS5yE0)
 - [Slides](https://pubhub.devnetcloud.com/media/nso/site/Slides-2020/NSODevDays2020-Demystifying-NSO-Commit-Queues.pdf)


They are about to get an understanding about various angles of commit-queues:

 - Using sync/async
 - Atomicity
 - Using device templates
 - Overlapping config
 - Bundling
 - Rollback On Error

I recommend seeing his presention before running the scenarios.

## Common scenario criterias:

 - devices global-settings commit-queue retry-attemts = unlimited
