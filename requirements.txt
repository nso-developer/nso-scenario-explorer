aiohttp>=3.8
flask==2.0.3
flask_cors==3.0.10
python-socketio>=5.0.2
requests==2.27.1
rich==9.11.0
Werkzeug==2.0.1
flask_socketio==5.1.2
psutil==5.9.0
