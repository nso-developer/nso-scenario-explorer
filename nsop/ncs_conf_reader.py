#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import sys
import xml.sax
from nso_notifications import PythonObjectHandler
from dict_dot import Map

class NCSConfParseError(Exception):
    pass

# Read ncs.conf into a python dictionary.
# If filename is a directory, assume ncs.conf is directly in that directory.
def read_ncs_conf(filename):
    parser = xml.sax.make_parser()
    handler = PythonObjectHandler()
    parser.setContentHandler(handler)
    if os.path.isdir(filename):
        filename = filename + os.path.sep + 'ncs.conf'
    try:
        parser.parse(filename)
    except StopIteration:
        return Map(handler.o)
    except xml.sax.SAXParseException:
        raise NCSConfParseError(f"Failed to parse NSO configuration file: "
                                f"{filename}")
    except ValueError:
        raise NCSConfParseError(f"Failed to read NSO configuration file: "
                                f"{filename}")

if __name__ == '__main__':
    cfg = read_ncs_conf(sys.argv[1])

    print(cfg.ncs_config.webui.idle_timeout)
    print(cfg.ncs_config.webui.transport.tcp.port)
    print(cfg.ncs_config.netconf_north_bound.transport.ssh.port)
