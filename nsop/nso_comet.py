#!/usr/bin/env python3

#
# Copyright 2013 Tail-f Systems
#
#(C) 2017 Tail-f Systems
#
#(C) 2021 Cisco Systems, Inc.

import sys
import argparse
import json
import subprocess
import nso_api
import time
import os
import pprint as pp

from nso_api import NsoSession as nso
from dict_dot import Map
#import nso_comet # This module

pprint = pp.PrettyPrinter(indent=4).pprint

#
# TODO:
#  - Support for list changed.
#  - Support for nested lists.
#  - Support for keyless lists.
#  - Filter on handle
#
class ChangedFilter:
    def __init__(self, path):
        self.path = path
    def process_event(self, event):
        for r in event.result:
            r = Map(r)
            if r.message.changes:
                for ch in r.message.changes:
                    ch = Map(ch)
#                    print(f"DEBUG {ch.op:15} {ch.keypath}")
                    if ch.keypath.startswith(self.path):
                        key = ch.keypath[len(self.path):]
                        if key.find("}")+1 == len(key):
                            if ch.op == "created":
                                self.created(key)
                            elif ch.op == "deleted":
                                self.deleted(key)
#                            else:
#                                print(f"Ignoring {ch.op:15} {ch.keypath}")
    def created(self, key):
        raise NotImplementedError
    def deleted(self, key):
        raise NotImplementedError

class UpdatedFilter:
    def __init__(self, path):
        self.path = path
    def process_event(self, event):
        updated = False
        for r in event.result:
            r = Map(r)
            if r.message.changes:
                for ch in r.message.changes:
                    ch = Map(ch)
#                    print(f"DEBUG {ch.op:15} {ch.keypath}")
                    if ch.keypath.startswith(self.path):
                        self.updated()
                        return

    def updated(self):
        raise NotImplementedError


class ServicesUpdated(ChangedFilter):
    def updated(self):
        print("Services changed")
    def created(self, key):
        print(f"Service created        {key}")
    def deleted(self, key):
        print(f"Service deleted        {key}")

class CommitQueueExecuting(UpdatedFilter):
    def updated(self):
        print("COMMIT-QUEUE changed")
    def created(self, key):
        print(f"CQ created        {key}")
    def deleted(self, key):
        print(f"CQ deleted        {key}")

class CommitQueueCompleted(ChangedFilter):
    def created(self, key):
        print(f"CC created        {key}")
    def deleted(self, key):
        print(f"CC deleted        {key}")

class AlarmList(UpdatedFilter):
    def updated(self):
        print("ALARMLIST changed")
    def created(self, key):
        print(f"AL created        {key}")
    def deleted(self, key):
        print(f"AL deleted        {key}")

def main(argv):
    nscc = nso('localhost', None, 'admin', 'admin', port=8080, debug=False)
    nscc.login('admin', 'admin')
    nscc.getNewTrans('read_write')

    handle = nscc.subscribe_changes("main", "/hostname:hostname")
    nscc.start_subscription(handle)
    handle = nscc.subscribe_cdboper("main", "/hostname:hostname")
    nscc.start_subscription(handle)
#    handle = nscc.subscribe_cdboper("main", "/alarms/alarm-list/alarm")
#    nscc.start_subscription(handle)
    handle = nscc.subscribe_cdboper("main", "/devices/commit-queue")
    nscc.start_subscription(handle)
#    handle = nscc.subscribe_cdboper("main", "/devices/commit-queue/completed")
#    nscc.start_subscription(handle)
    services = ServicesUpdated("/hostname:hostname")
    cq_exec = CommitQueueExecuting("/ncs:devices/commit-queue/qitem")
    cq_comp = CommitQueueCompleted("/ncs:devices/commit-queue/completed/queue-item")
    alarmlist = AlarmList("/al:alarms/alarm-list/alarm")
    while True:
        data = nscc.comet("main")
        if data is not None:
            event = Map(data)
            if event.error.type == "session.invalid_sessionid":
                print("Invalid sessionid")
                break
            if event.result:
                services.process_event(event)
                cq_exec.process_event(event)
                cq_comp.process_event(event)
                alarmlist.process_event(event)
                print("======================================================================================================")
#                for r in event.result:
#                    r = Map(r)
#                    if r.message.changes:
#                        for ch in r.message.changes:
#                            ch = Map(ch)
#                            print(f"{ch.op:15} {ch.keypath}")


    nscc.logout()

"""
Nothing has happend:
 {'jsonrpc': '2.0', 'result': [], 'id': 1}
Subscribe config change:
 {'jsonrpc': '2.0', 'result': [{'handle': '1', 'message': {'db': 'running', 'changes': [{'keypath': '/hostname:hostname{ulrik}', 'op': 'created', 'value': ''}], 'user': 'admin', 'ip': '127.0.0.1'}}], 'id': 1
Invalid session id:
 {'jsonrpc': '2.0', 'error': {'type': 'session.invalid_sessionid', 'code': -32000, 'message': 'Invalid sessionid'}, 'id': 1}
"""

if __name__ == '__main__' :
    main(sys.argv[1:])
