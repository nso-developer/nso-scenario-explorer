#!/usr/bin/env python

"""
TODO:
 - Check return code to determine problem at startup and retry
"""

import asyncio
from base64 import b64encode
import json
import os
import sys
import time
import threading

import aiohttp

from dict_dot import Map

HEADERS_STREAM={
    'Accept':'text/event-stream',
    'Accept-Encoding': 'identity', # Prevent NSO from gzipping data and delaying
                                   # sending of events.
}

def add_authentication(h, user, password):
    credentials = f'{user}:{password}'.encode('ascii')
    h['Authorization'] = 'Basic %s' % b64encode(credentials).decode("ascii")


notifications = None
list_notifications = None
updated_cb = None

def add_notification(msg):
    stream, notif = msg
    e = Map(notif)
    list_notifications[stream].append(notif)
    if updated_cb is not None:
      updated_cb(stream)

def replace_uc_and_ns(o):
    if type(o) is dict:
        no = {}
        for k,v in o.items():
            nk = k.replace('-', '_')
            if ':' in nk:
                _,nk = nk.split(':')
            no[nk] = replace_uc_and_ns(v)
        return no
    elif type(o) is list:
        return [ replace_uc_and_ns(e) for e in o ]
    else:
        return o

#
# Parse json to a dict object.
# Dash is replaced with _ for keys to allow for dot access to all keys.
# Namespace is removed to keep compatibility with NETCONF code.
#
async def get_stream(stream, host='localhost', port=8080,
                     user='admin',
                     password='admin'):
    conn = aiohttp.TCPConnector() # No limit of parallel connections
    client = aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(sock_read=0), connector=conn)
    url = f'http://{host}:{port}/restconf/streams/{stream}/json'
    print("get_stream", url)
    headers = HEADERS_STREAM.copy()
    add_authentication(headers, user, password)
    async with client.get(url, headers=headers) as response:
        assert response.status == 200
        s = ""
        try:
            async for line in response.content:
                l = line.decode('utf-8').strip()
                if l == "": # Assuming an emply lines is end of message
                    o = json.loads(s)
                    no = replace_uc_and_ns(o)
                    #print(no)
                    add_notification((stream, no))
                    s = ""
                else:
                    assert(l[:6] == 'data: ')
                    s += l[6:]
        except Exception as s:
            print(s)
    await client.close()


"""
{'notification': {'eventTime': '2021-03-01T19:03:38.735607+01:00',
                  'ncs_commit_queue-progress_event': {'id': '1614621818090',
                                                      'state': 'deleted',
                                                      'tag': 'commit:419'}}}
"""

class RESTCONFNotifications(threading.Thread):
    def __init__(self, host='localhost', port=8080,
                       user='admin', password='admin',
                       streams=None, cb=None):
        self.host=host
        self.port=port
        self.user=user
        self.password=password
        self.streams = streams
        self.cb=cb
        super().__init__()
        self.running = False

    async def start_streams(self):
        await asyncio.gather(*[ get_stream(stream, self.host, self.port,
                                           self.user, self.password)
                  for stream in self.streams ])

    def run(self):
        # Process 
        self.running = True
        while self.running: # Retry loop
            asyncio.run(self.start_streams())
            print("subscription(s) stopped.")
            if self.running:
                time.sleep(3) # Retry delay

    def stop(self):
        self.running = False

def purge(stream):
    global list_notifications
    list_notifications[stream] = []
    if updated_cb is not None:
        updated_cb(stream)

def start_notifications(host='localhost', port=8080,
                        user='admin', password='admin',
                        streams=None, cb=None):
    global notifications, list_notifications, updated_cb
    streams = streams or ['ncs-events']
    if isinstance(streams, str) or not hasattr(streams, '__iter__'):
        streams = [streams]
    list_notifications = { stream: [] for stream in streams}
    updated_cb = cb
    if notifications is None:
        notifications = RESTCONFNotifications(host=host, port=port,
                                              user=user, password=password,
                                              streams=streams,
                                              cb=add_notification)
        notifications.start()

def stop_notifications():
    global notifications
    if notifications is not None:
        notifications.stop()

def get_notifications(stream):
    try:
        return list(list_notifications[stream])
    except KeyError:
        return []

# Used when running on command line
def updated(stream):
    print(list_notifications[stream][-1:])

if __name__ == '__main__':
    start_notifications(host='localhost', port=8080,
                        user='admin', password='admin',
                        streams = ['ncs-events', 'service-state-changes'],
                        cb=updated)
    notifications.join()
