#
# Copyright 2013 Tail-f Systems
#
#(C) 2017 Tail-f Systems

import json
import nso_api


class Alarms:

    def __init__(self, session):
      self.nso = session
      return


    def purge(self):
        action = "/alarms/purge-alarms"
        params = { 'alarm-status': 'any' }
        results = self.nso.action(action, params)
        return results
