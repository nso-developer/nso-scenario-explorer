#!/usr/bin/env python

import os
import select
import subprocess
import sys
import time
import threading
import xml.sax
from xml.sax import SAXParseException
import xml.sax.expatreader

from dict_dot import Map


notifications = None
list_notifications = None
updated_cb = None

def add_notification(msg):
    stream, notif = msg
    e = Map(notif)
    if 'rpc_reply' in e:
      if 'ok' in e.rpc_reply:
          print(f"Successfully subscribing for {stream} notifications.")
      else:
          print(f"Failed subscribing for {stream} notifications.")
    else:
      list_notifications[stream].append(notif)
      if updated_cb is not None:
          updated_cb(stream)

#
# Parse XML to a dict object.
# Dash is replaced with _ for keys to allow for dot access to all keys.
#
"""
{'notification': {'eventTime': '2021-03-01T19:03:38.735607+01:00',
                  'ncs_commit_queue-progress_event': {'id': '1614621818090',
                                                      'state': 'deleted',
                                                      'tag': 'commit:419'}}}
"""
class PythonObjectHandler(xml.sax.handler.ContentHandler):
  def startDocument(self):
    self.lo = []
    self.o = {}

  def startElement(self, name, attrs):
      name = name.replace('-', '_')
      o = {}
      if name not in self.o:
        self.o[name] = o
      else:
        l = self.o[name]
        if not isinstance(l, list):
          l = [l]
          self.o[name] = l
        l.append(o)
      self.lo.append(self.o)
      self.o = o

  def endElement(self, name):
    self.o = self.lo.pop()
    if not self.lo:
      raise StopIteration

  def characters(self, content):
    c = content.strip()
    if c:
        self.o['text'] = c

# Read line by line and skip empty lines...
# Skip all lines until <?xml is found
class NCParser(xml.sax.expatreader.ExpatParser):
    def __init__(self):
      super().__init__()
      self.buf = bytearray()
    def has_data_to_process(self):
        return self.buf and self.buf.find(b'\n') != -1
    def resetx(self):
      self.reset()
    def feedx(self, buf):
      self.buf += buf
      while True:
        nl = self.buf.find(b'\n')
        if nl == -1:
          return
        line = self.buf[:nl].strip()
        self.buf = self.buf[nl+1:]
        if len(line):
          if self._parsing or line.startswith(b'<?xml'):
              self.feed(line)


class NETCONFNotifications(threading.Thread):
    def __init__(self, host='localhost', port=2022, streams=None, cb=None):
        self.host=host
        self.port=port
        self.streams = streams
        self.cb=cb
        super().__init__()
        self.running = False
        self.nc = {}
        self.poller = select.poll()


    def start_stream(self, stream):
        return subprocess.Popen(['netconf-console', '-s', 'raw', '--port',
                            str(self.port), f'--create-subscription={stream}'],
                            stdout=subprocess.PIPE)

    def start_streams(self):
        for stream in self.streams:
            # Create XML parser
            parser = NCParser()
            handler = PythonObjectHandler()
            parser.setContentHandler(handler)
            nc = self.start_stream(stream)
            self.nc[nc.stdout.fileno()] = (stream, parser, handler, nc)
            self.poller.register(nc.stdout)

    def stop_streams(self):
        if self.nc:
            for fd,(_,_,_,nc) in self.nc.items():
                nc.kill()
                self.poller.unregister(fd)
            self.nc = {}

    def run(self):
        # Process 
        self.running = True
        try:
            while self.running: # Retry loop
                self.start_streams()
                print(f"netconf-console(s) starting to {self.host}:{self.port}")
                alive = True
                while alive: # Processing loop
                    events = self.poller.poll(100)
                    if events:
                        for fd,event in events:
                            if event & select.POLLHUP:
                                alive = False
                                break
                            if event & select.POLLIN:
                                stream, parser, handler, nc = self.nc[fd]
                                l = os.read(fd, 1024)
                                while len(l) or parser.has_data_to_process():
                                    try:
                                        l_to_process = l
                                        l = bytes()
                                        parser.feedx(l_to_process)
                                    except SAXParseException as e:
                                        print("Error Parsing netconf-console"
                                              "notification", l)
                                        print(e)
                                    except StopIteration:
                                        if self.cb:
                                            self.cb((stream, handler.o))
                                        parser.resetx()
                    else:
                        pass # Poll timeout

                print("netconf-console(s) stopped.")
                if self.running:
                    time.sleep(3) # Retry delay
                self.stop_streams()
        finally:
            self.stop_streams()

    def stop(self):
        self.running = False
        self.stop_streams()

def purge(stream):
    global list_notifications
    list_notifications[stream] = []
    if updated_cb is not None:
        updated_cb(stream)

def start_notifications(port=2022, streams=None, cb=None):
    global notifications, list_notifications, updated_cb
    streams = streams or ['ncs-events']
    if isinstance(streams, str) or not hasattr(streams, '__iter__'):
        streams = [streams]
    list_notifications = { stream: [] for stream in streams}
    updated_cb = cb
    if notifications is None:
        notifications = NETCONFNotifications(port=port, streams=streams,
                                             cb=add_notification)
        notifications.start()

def stop_notifications():
    global notifications
    if notifications is not None:
        notifications.stop()

def get_notifications(stream):
    try:
        return list(list_notifications[stream])
    except KeyError:
        return []

# Used when running on command line
def updated(stream):
    print(list_notifications[stream][-1:])

if __name__ == '__main__':
    start_notifications(port=2022, streams=['service-state-changes', 'ncs-events'], cb=updated)
    notifications.join()
