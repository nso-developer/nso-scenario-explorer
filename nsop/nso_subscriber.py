from queue import Empty, Queue
import logging
import threading
import time

import requests

from dict_dot import Map
from nso_api import NsoSession as nso
import nso_comet

logger = logging.getLogger(__name__)

q = Queue()
subscribers = None


class ServicesUpdated(nso_comet.UpdatedFilter):
    def updated(self):
        logger.info("Services changed")
        q.put('tabledata_services_updated')

class CommitQueueExecuting(nso_comet.UpdatedFilter):
    def updated(self):
        logger.info("CQ EXEC changed")
        q.put('tabledata_executing_updated')

class CommitQueueCompleted(nso_comet.UpdatedFilter):
    def updated(self):
        global q
        logger.info("CQ COMPLETED COMMIT-QUEUE changed")
        q.put('tabledata_completed_updated')

class AlarmListUpdated(nso_comet.UpdatedFilter):
    def updated(self):
        logger.info("ALARMLIST changed")
        q.put('tabledata_alarms_updated')


def subscribe_oper(nscc, cls, path):
    srvc = None
    try:
        nscc.start_subscription(nscc.subscribe_cdboper("main", path))
        if cls is not None:
            srvc = cls(path)
    except Exception as e:
        print(f"WARNING: Failed to subscribe to path: {path}")
        print(e)
    return srvc

def subscribe_oper_and_config(nscc, cls, path):
    srvc = None
    try:
        nscc.start_subscription(nscc.subscribe_changes("main", path))
        nscc.start_subscription(nscc.subscribe_cdboper("main", path))
        if cls is not None:
            srvc = cls(path)
    except Exception as e:
        print(f"WARNING: Failed to subscribe to path: {path}")
        print(e)
    return srvc

nscc = {}
class NSOSubscribers(threading.Thread):
    def __init__(self, port=8080):
        self.port=port
        super().__init__()
        self.running = False
    def run(self):
        global nscc

        self.running = True
        while self.running:
            logger.info(f"Starting subscriber to localhost:{self.port}")
            try:
                nscc = nso('localhost', None, 'admin', 'admin', port=self.port, debug=False)
                nscc.login('admin', 'admin')

                sub_filters = []
                services = nscc.get_service_points().keys()

                for service in services:
                    logger.info(f"Subscribing for changes to {service}")
                    sub_filters.append(subscribe_oper_and_config(nscc,
                                    ServicesUpdated, service))

                sub_filters.append(subscribe_oper(nscc, AlarmListUpdated,
                                   "/al:alarms/alarm-list/alarm"))
                subscribe_oper(nscc, None,
                                   "/ncs:devices/commit-queue")
                sub_filters.append(CommitQueueExecuting("/ncs:devices/commit-queue/qitem"))
                sub_filters.append(CommitQueueCompleted("/ncs:devices/commit-queue/completed/queue-item"))

                sub_filters = list(filter(lambda e: e is not None, sub_filters))

                while True:
                    data = nscc.comet("main")
                    if data is not None:
                        event = Map(data)
                        if event.error.type == "session.invalid_sessionid":
                            logger.info("Invalid sessionid")
                            break
                        if event.result:
                            for f in sub_filters:
                                f.process_event(event)
                logger.info("Stopping subscriber.")
            except requests.exceptions.ConnectionError:
                print("Failed to setup subscriber, retrying...")
                time.sleep(3)

    def stop(self):
        self.running = False

def get_event():
    try:
        return q.get()
    except Empty:
        pass
    return None


def start_subscribers(port=8080):
    global subscribers
    if subscribers is None:
        subscribers = NSOSubscribers(port)
        subscribers.start()

def stop_subscribers():
    global subscribers
    if subscribers is not None:
        subscribers.stop()
