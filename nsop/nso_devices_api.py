#
# Copyright 2013 Tail-f Systems
#
#(C) 2017 Tail-f Systems

import json
import nso_api

nedIdMapping = {'netconf' : {'netconf' : 'netconf' },
                'Cisco IOS-XR' : {'type' : 'cli', 'ned-id' : 'cisco-ios-xr-id:cisco-ios-xr'},
                'Cisco IOS' : {'type' : 'cli', 'ned-id' : 'ios-id:cisco-ios'},
                'Cisco Nexus' : {'type' : 'cli', 'ned-id' : 'cisco-nx-id:cisco-nx'},
                'Cisco ASA' : {'type' : 'cli', 'ned-id' : 'asa-id:cisco-asa'},
                'ALU' : {'type' : 'cli', 'ned-id' : 'alu-sr-id:alu-sr'},
                'Arista' : {'type' : 'cli', 'ned-id' : 'arista-dcs-id:arista-dcs'},
               }

class nsoDevices:

    def __init__(self, session):
      self.nso = session
      return

    @staticmethod
    def getNedInfo():
      return nedIdMapping


    def devicesAction(self, act, devices, result_for_ui=False):

       dev = devices.split(',')
       res = ''

       if act in ['sync-from', 'check-sync', 'sync-to', 'connect']:

          dev = devices.split(',')
          if (dev[0] == 'all'):
            dev.pop(0)
          action = '/ncs:devices/' + act
          results = self.nso.action(action, {'device': dev}, fmt='json')
          print(results)

          if result_for_ui:
              tr = False
              for d in results['result']['sync-result']:
                  res += "<tr>"
                  res += "<td>" + d['device'] + "</td>"
                  if d['result'] in ['true', 'in-sync']:
                      res += '<td><span class=\"label label-success\">' + d['result'] + '</span></td>'
                  else:
                      res += '<td><span class=\"label label-danger\">' + d['result'] + '</span></td>'
                  res += "<td>" + d.get('info', '') + "</td>"
                  res += "</tr>"
              return res

          return results

       elif act == 'compare-config':

            for dn in dev:
              results = self.nso.action('/ncs:devices/ncs:device{' + dn + "}/compare-config", fmt='json')

              if 'result' in results:
                if not results['result']:
                  res += '\n*** no diff detected for device %s ***\n' % dn
                elif results['result'].get('diff'):
                  res += f"\n========= {dn} ==========\n"
                  res += results['result'].get('diff')
                elif results['result'].get('diff'):
                  res += f"\n========= {dn} ==========\n"
                  res += results['result'].get('diff')

            return res

       return {'error' : 'command failed to execute'}


    def deviceAdd(self, devParams):

       dn = devParams['name']
       dt = devParams['devtype']
       path = '/ncs:devices/device{' + dn + '}'

       if self.nso.exists(path) == False :
          result = self.nso.create(path)
          return('Failed: device already exists')

       self.nso.set(path + '/address', devParams['address'])
       self.nso.set(path + '/port', devParams['port'])
       self.nso.set(path + '/authgroup', devParams['authgroup'])
       self.nso.set(path + '/state/admin-state', devParams['state'])

       if (devParams['devtype']  == 'netconf'):
         self.nso.create(path + '/device-type/netconf')
       else:
         self.nso.create(path + '/device-type/'+ nedIdMapping[dt]['type'])
         self.nso.set(path + nedIdMapping[dt]['type'] + '/ned-id', 
                      nedIdMapping[dt]['ned-id'])

       return 'ok'

    def deviceDel(self, devName):

       if self.nso.exists(path) == False :
          result = self.nso.create(path)
          return('Failed: device does not exists')

       self.nso.delete
       return

