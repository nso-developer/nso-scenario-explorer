#
# Copyright 2013 Tail-f Systems
#
#(C) 2017 Tail-f Systems

import json
import logging

import nso_api


logger = logging.getLogger(__name__)
already_logged = False


class Services:
    def __init__(self, session):
        self.nso = session

    def getServices(self):
        global already_logged
        srvc_types = []
        for sp in self.nso.get_service_points():
            sch = self.nso.get_schema("", sp, levels=1)
            keys = []
            for c in sch['children']:
                if c['kind'] == 'key':
                    keys.append(c['name'])
            srvc_types.append((sp, keys))
        srvcs = []
        for sp, keys in srvc_types:
            if len(keys):
                srvcs += self.getServiceGeneric(sp, keys)
            else:
                if not already_logged:
                    logger.error(f"Service point '{sp}' has no keys.")
                    already_logged = True
        return srvcs

    def getServiceGeneric(self, spath, keys):
        data = self.nso.simple_query(None,
                                     spath,
                                     keys)
        # TODO: Handle multipe keys transformation to name
        srvcs = [{'name': ' '.join(values)} for values in data['result']['results']]
        for s in srvcs:
           s['type'] = spath
           #hpath = f"{spath}{{\"{s['name']}\"}}"
           name = s['name']
           name = name.replace("'", "\\'")
           name = name.replace("\"", "\\\"")
           hpath = f"{spath}{{{name}}}"
           s['devices'] = ','.join(self.nso.getValue(hpath+"/modified/devices") or [])
           cqc = self.nso.simple_query(hpath, "commit-queue/queue-item", ['id'])
           err = cqc.get('error')
           if err:
                if err.get('type') == 'rpc.method.failed':
                    logger.warning(f"Service was removed {hpath}")
                    continue
           cqids  = [item for sublist in cqc['result']['results'] for item in sublist]
           s['commit-queue'] = ','.join(cqids)
        return srvcs



    def getHostname(self):
        query_info = ['name', 'dummy']
        spath = "/hostname"
        data = self.nso.simple_query(None,
                                     "hostname",
                                     query_info)
        srvcs = [dict(zip(query_info, values)) for values in data['result']['results']]
        for s in srvcs:
           hpath = f"{spath}{{{s['name']}}}"
           s['devices'] = ','.join(self.nso.getValue(hpath + "/device"))
           cqc = self.nso.simple_query(hpath, "commit-queue/queue-item", ['id'])
           cqids  = [item for sublist in cqc['result']['results'] for item in sublist]
           s['commit-queue'] = ','.join(cqids)
           s['type'] = spath
        return srvcs


    def getIntf(self):
        query_info = ['name']
        spath = "/intf"
        data = self.nso.simple_query(None,
                                     "intf",
                                     query_info)
        srvcs = [dict(zip(query_info, values)) for values in data['result']['results']]
        for s in srvcs:
           hpath = f"{spath}{{{s['name']}}}"
           s['devices'] = self.nso.getValue(hpath + "/device")
           cqc = self.nso.simple_query(hpath, "commit-queue/queue-item", ['id'])
           cqids  = [item for sublist in cqc['result']['results'] for item in sublist]
           s['commit-queue'] = ','.join(cqids)
           s['type'] = spath
        return srvcs


    def getTop(self):
        query_info = ['name']
        spath = "/top"
        data = self.nso.simple_query(None,
                                     spath,
                                     query_info)
        srvcs = [dict(zip(query_info, values)) for values in data['result']['results']]
        for s in srvcs:
           hpath = f"{spath}{{{s['name']}}}"
           device = self.nso.getValue(hpath + "/device")
           s['devices'] = ','.join(device)
           cqc = self.nso.simple_query(hpath, "commit-queue/queue-item", ['id'])
           cqids  = [item for sublist in cqc['result']['results'] for item in sublist]
           s['commit-queue'] = ','.join(cqids)
           s['type'] = spath
        return srvcs


    def getCfsVlan(self):
        query_info = ['name']
        spath = "/cfs-vlan"
        data = self.nso.simple_query(None,
                                     "cfs-vlan",
                                     query_info)
        srvcs = [dict(zip(query_info, values)) for values in data['result']['results']]
        for s in srvcs:
           hpath = f"{spath}{{{s['name']}}}"
           a_router = self.nso.getValue(hpath + "/a-router")
           z_router = self.nso.getValue(hpath + "/z-router")
           s['devices'] = a_router + ',' + z_router
           cqc = self.nso.simple_query(hpath, "commit-queue/queue-item", ['id'])
           cqids  = [item for sublist in cqc['result']['results'] for item in sublist]
           s['commit-queue'] = ','.join(cqids)
           s['type'] = spath
        return srvcs


    def getRfsVlan(self):
        query_info = ['name']
        spath = "/rfs-vlan:vlan"
        data = self.nso.simple_query(None,
                                     spath,
                                     query_info)
        srvcs = [dict(zip(query_info, values)) for values in data['result']['results']]
        for s in srvcs:
           hpath = f"{spath}{{{s['name']}}}"
           router = self.nso.getValue(hpath + "/router")
           s['devices'] = router
           cqc = self.nso.simple_query(hpath, "commit-queue/queue-item", ['id'])
           cqids  = [item for sublist in cqc['result']['results'] for item in sublist]
           s['commit-queue'] = ','.join(cqids)
           s['type'] = spath
        return srvcs


    def getProduitAcces(self):
        query_info = ['num-prestation']
        spath = '/p-acces:produit-acces'
        data = self.nso.simple_query(None,
                                     spath,
                                     query_info)
        srvcs = [dict(zip(['name'], values)) for values in data['result']['results']]
        for s in srvcs:
           hpath = f"{spath}{{{s['name']}}}"
           ds_device = self.nso.getValue(hpath + "/downstream-device")
           us_device = self.nso.getValue(hpath + "/upstream-device")
           s['devices'] = ds_device+','+us_device
           cqc = self.nso.simple_query(hpath, "commit-queue/queue-item", ['id'])
           cqids  = [item for sublist in cqc['result']['results'] for item in sublist]
           s['commit-queue'] = ','.join(cqids)
           s['type'] = spath
        return srvcs


    def getCfsEas(self):
        query_info = ['device']
        spath = '/c-eas:cfs-eas'
        data = self.nso.simple_query(None,
                                     spath,
                                     query_info)
        srvcs = [dict(zip(['name'], values)) for values in data['result']['results']]
        for s in srvcs:
           hpath = f"{spath}{{{s['name']}}}"
           s['devices'] = s['name']
           cqc = self.nso.simple_query(hpath, "commit-queue/queue-item", ['id'])
           cqids  = [item for sublist in cqc['result']['results'] for item in sublist]
           s['commit-queue'] = ','.join(cqids)
           s['type'] = spath
        return srvcs


    def getRfsEas(self):
        query_info = ['device']
        spath = '/r-eas:rfs-eas'
        data = self.nso.simple_query(None,
                                     spath,
                                     query_info)
        srvcs = [dict(zip(['name'], values)) for values in data['result']['results']]
        for s in srvcs:
           hpath = f"{spath}{{{s['name']}}}"
           s['devices'] = s['name']
           cqc = self.nso.simple_query(hpath, "commit-queue/queue-item", ['id'])
           cqids  = [item for sublist in cqc['result']['results'] for item in sublist]
           s['commit-queue'] = ','.join(cqids)
           s['type'] = spath
        return srvcs
