# NSO Scenario Explorer

A lab environment where to explore and learn the behavior of NSO.

Currently it contains a number of scenarios to explore service creation with
commit queues in a single node setup or using LSA.


## Purpose

The purpose of this environment is provide examples of how to utilize NSO
commit queues and get an understanding of how they do really work.

A set of tools and prepared scenarios provided in the repository can be used
as help to better comprehend how transactions and notifications are propageted
in the system.

All of the examples uses a NSO local installation with default settings
and all operations are done using the JSON RPC API. Notifications are
subscribed using the RESTCONF API.


## Tools

Tool | Description
---- | -----------
netsim_controller.py | Python module/script that attaches a two-phase subscriber with keypath '/' and asks if the ongoing transaction should be accepted or rejected.
netsim-control-panel | Curses based Python script to start/stop and control the transaction in one or more instances of netsim and/or NSO.
nso-portal | Flasked based web portal to provide a single view for services, commit queues, alarms, commit queue progress events and service plan notifications.
nso-scenario | Python script to run scenarios.

## Scenario collections

The prepared scenarios are grouped into collections. Each collection is located
in the directories with it's name starting with 'scenarios' and the default
collctions directory used is 'scenarios'.

Directory | Description
--------- | -----------
senarios | Scenarios exploring commit queues with simple services.
senarios-rfm | Scenarios exploring commit queues with reactive FASTMAP based services.
senarios-nano | Scenarios exploring commit queues with nano services.
senarios-lsa | Scenarios exploring commit queues with simple services in an LSA system.
senarios-lsa-rfm | Scenarios exploring commit queues with reactive FASTMAP based services in an LSA system.
senarios-lsa-nano | Scenarios exploring commit queues with nano services in an LSA system.
senarios-transient | Scenarios exploring commit queues to handle transient communication errors.
senarios-commit-queue | The initial inspiring scenarios from Dan Sullivan's demo from developer's days 2020.
senarios-test | Scenarios for testing individual scenario commands. Hopefully a complete collection in the future.

## Documentation

Apart from this file, you find more detailed documentaion for the individual
commands:

 * [netsim-control-panel](README.netsim-control-panel.md)
 * [nso-scenario](README.nso-scenario.md)
 * [nso-portal](nsop/README.md)

And the main documentation is the code itself.


## Dependencies

In order to utilize all the functionality, you will need to have (in the path)
the following components.

* NSO 5.2+ (Probably works with older versions. The portal does not work
  properly with 4.7.x)
* Python 3.6+
* Additional Python module dependencies are specified in requirements.txt. A
  virtual envifonment is created in directory venv by the Makefile.


## Build instructions

The scenario explorer demo can be setup in both single node or lsa, by running
one of the following commands.



### Single node environment

From the top level directory simply type the following

```
   . <path-to-nso>/ncsrc
   make single
   . venv/bin/activate
```


### LSA environment

From the top level directory simply type the following

```
   . <path-to-nso>/ncsrc
   make lsa
   . venv/bin/activate
```


## Running scenarios

### Start up the demo environment
First start up the simulation and the netsim devices

```
  make start
```

For a more visual and intuitive understanding of what is happening you can
start the NSO portal in a separate termial (can be minimized after start) ...

```
  . venv/bin/activate
  nsop/nso-portal --ncs-conf . --autologin commitqueue-demo
```

and the NSO Netsim Control Panel in a third terminal
```
./netsim-control-panel
```

You can log into the portal by using the following URL: http://localhost:4000

The '--autologin' option bypasses the login and redirects to the page
and redirects to the 'commitqueue-demo' page.

NOTE! The commit queue demo page does auto upate so you don't need to
refresh it to see updates.


### Restore initial state

Before running the tests it is a good practice to restore the initial state and
be sure that there a no created service instances and/or devices that are
out-of-sync:

```
./cleanup.sh
```

*NOTE!* This script is fairly good in restoring the initial state, but some
circumstances might prevent it from doing so, e.g. a device is in locked state
and can not be synced from.


### List scenarios

By default the scenarios in directory *scenarios* are listed. Scenarios in other
directories can be used by the -d option.
```
❯ ./nso-scenario list

		Current Loaded Scenarios
		--------------------------
		1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR
		10-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-NOT-RESP
		11-STACKED-SERVICES-CQ-STOP-ON-ERROR-ROLLBACK-NOT-RESP
		2-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-NON-ATOMIC
		3-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR
		4-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR-NON-ATOMIC
		5-STACKED-SERVICES-CQ-STOP-ON-ERROR
		6-STACKED-SERVICES-CQ-STOP-ON-ERROR-NON-ATOMIC
		7-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-RESP
		8-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-RESP
		9-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-NOT-RESP
		SIMPLE-SERVICE-CQ-ROLLBACK-ON-ERROR
		SIMPLE-SERVICE-NOCQ-FAILING-DEVICE

```


### Select a particular scenario and run it
```
❯ ./nso-scenario run 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR

Starting NSO Commit Queue Rollback Atomic

	Step(1): Stop netsim device ex0..........................................................Complete
	Step(2): Create TOP Service Instance(A) [ex0, ex1].......................................Complete

                <config xmlns="http://tail-f.com/ns/config/1.0">
                  <top xmlns="http://example.com/top">
                    <name>A</name>
                    <stacked-devices>ex0</stacked-devices>
                    <stacked-devices>ex1</stacked-devices>
                    <vid>100</vid>
                  </top>
                </config>

	Step(3): Commit Changes (A)..............................................................Complete

		Commit Option Settings
		---------------------------------------------
		commit-queue              : async
		commit-queue-atomic       : true
		commit-queue-error-option : rollback-on-error
		commit-queue-tag          : Service (A)

                commit-queue id: 1644923179233

	Step(4): Service instances...............................................................Complete

                /top:top                                         name
                                                                ────────────────────────────────
                                                                 A


                /rfs-vlan:services/vlan                          name
                                                                ────────────────────────────────
                                                                 ex0-A
                                                                 ex1-A

	Step(5): Display Commit Queue(s).........................................................Complete

                 ID              Tag           Age   Status      Atomic   Device(s)
                ────────────────────────────────────────────────────────────────────
                 1644923179233   Service (A)   0     executing   true     ex0,ex1

	Step(6): Check commit queue(s)...........................................................Complete

                Verify commit-queue(s) and hit return to continue:

	Step(7): Wait for commit queues to drain.................................................Complete

                 ID            Tag   Age   Status   Atomic   Device(s)
                ───────────────────────────────────────────────────────
                 ** Empty **

	Step(8): Display Commit Queue Complete...................................................Complete

                 ID              Tag           Status   Failed Device(s)   Completed Device(s)
                ───────────────────────────────────────────────────────────────────────────────
                 1644923179233   Service (A)   failed   ex0                ex0,ex1

	Step(9): Service instances...............................................................Complete

                /top:top                                         name
                                                                ────────────────────────────────
                                                                 ** No Services **


                /rfs-vlan:services/vlan                          name
                                                                ────────────────────────────────
                                                                 ** No Services **

	Step(10): Check commit queue(s)..........................................................Complete

                Observe if Rollback has occurred and hit return to continue:

	Step(11): Start netsim device ex0........................................................Complete
	Step(12): Check device sync status.......................................................Complete

                 Device                 Result           Info
                ───────────────────────────────────────────────────────────────────────────────────────
                 ex0                    in-sync
                 ex1                    in-sync

	Step(13): Cleanup (delete TOP service instance) .........................................Complete
	Step(14): Commit Cleanup.................................................................Complete

Finished NSO Commit Queue Rollback Atomic
```

## Running test suites

A number a pre-selected scenarios can be run for the current setup (single/LSA) by
running:

```
make test
```

It runs the pre-selected senarios for all service types (simple/RFM/nano)
without interuption. If a scenario fails due to a mismatch in pattern or a
failed call the execution is aborted with a failure.

Before running the tests it is a good practice to restore the initial state and
be sure that there are no created service instances and/or devices out-of-sync.

*NOTE!* These tests do not provide full coverage of NSO:s behavior when
creating services and using commit-queues, but gives a correct result when it
comes to check service instances and check-sync results and when the input
state is as expected.


## Clean up environment

To build a different environment setup you need to stop and remove the current
environment.

```
make stop clean
```
