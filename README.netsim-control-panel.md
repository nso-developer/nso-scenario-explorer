# NSO Netsim Control Panel

## Overview

NSO Netsim Control Panel is a simple tool that uses the netsim_controller Python
module to connect to the configured netsim instances and one or more NSO
instances.

When connected it sets up a two-phase subscriber to be notified and be able to
intervene on ongoing transactions.

This can be used for:
 - Asked for acceptance on transactions and the accept or reject them.
 - Set delay on transactions.

*NOTE!* It uses the NSO Python API and must be run in the same node as NSO and
netsim instances it connects to.

## netsim_controller Python module

This module connects to the ConfD or NSO instance and sets up the two-phase
subscriber.

It can be run stand-alone on the command line:

```
❯ ./netsim_controller.py r01
NSO Netsim Controller
Press:
  'q'               to quit.
  'm'               to toggle approval mode.
  'y/space/return'  to accept a transaction.
  'n'               to deny a transaction.
  's'               to start/stop the netsim.

Controller has started.
Transactions are asked for approval.
Connected to netsim.
```

or be used in by other Python scripts, i.e. NSO Netsim Control Panel:

```
import netsim_controller

controller = TransactionController(args.netsim, port)
controller.start()

# Communicate using the inqueue and outqueue
controller.inqueue.put(('delay', 2.0))
msg = controller.outqueue.get()
```

It subscribes to keypath '/', meaning that all transactions will be intercepted.

When the name of a netsim instance is specified, its IPC port is looked up
using:
```
ncs-netsim get-port <name>
```

Connecting to an NSO instance must be done using the '-p' option.

## NSO Netsim Control Panel

Implements a curses based UI on top of the netsim_controller module.

![Screenshot](doc/images/netsim-control-panel.png)

It is started with the command:
```
./netsim-control-panel
```
*Fun fact: The name is just a relic from the ideas to control transactions in/to
netsim instances. Then it seemed to be just as useful for NSO.*

### UI symbols

Symbol | Description
------ | -----------
A | Acceptance mode in on.
T | There is on ongoing transaction towards this instance.
Number e.g. 1.0 | Delay of transactions in seconds.
Accept or Reject | The ongoing transaction is waiting for command how to proceed.
Name color | The instance is running (green)/not running (white)

### Configuration
The netsim instances IPC ports are looked up using:
```
ncs-netsim list
```

The NSO instances need to be specified in the **netsim-control-panel.conf** file:
```
{
  "devices": {
    "lower-nso-1": {
      "port": 4569,
      "path": [
        "servicetypes"
      ]
    },
    "upper-nso": {
      "port": 4570,
      "path": [
        "servicetypes"
      ]
    }
  }
}
```

The variable *path* specifiled which paths that should be subscribed. The
special keyword **servicetypes** instructs it to get the registered servicetypes
by calling the action */ncs:services/get-servicetypes* implemented by the
package service-info (included in this demo) for NSO 4/5.x or reading tne
/ncs:services/service-type list in NSO 6.x.

Service types can manually be added to the list if the service-info
package is not installed. It is also possible to omit the servicetypes keyword
and manually specify all wanted paths, if a limited scope is needed.

### Keybindings ###

Press **h** to show/hide the help menu:

![Screenshot](doc/images/keybindings.png)

Use the arrow keys to select the instance to send a command to.

Accept/Reject are only valid when the text *Accept or Reject* is
shown.

