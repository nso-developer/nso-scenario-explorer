# Overview

This collection of scenarions used for a workshop at the Automation
Developer Days 2022 in Stockholm Sweden.

## Scenarios

 1. Create a service without using commit queues.
 2. Create a reactive fastmap service without using commit queues.
 3. Create a service with using commit queues (atomic=true, rollback-on-error)
 4. Create a service with using commit queues (atomic=false, rollback-on-error)
 5. Create a service with using commit queues (atomic=true, continue-on-error)
 6. Create a service with using commit queues (atomic=false, continue-on-error)
 6. Create a reactive fastmap service with using commit queues (parameterized)
