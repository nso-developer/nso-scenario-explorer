#!/bin/sh

set -e

# Allow for netsim to start properly
sleep 2

ncs_cli -u admin <<EOF
request devices fetch-ssh-host-keys
request devices sync-from
exit
exit
EOF




