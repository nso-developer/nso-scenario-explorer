#!/usr/bin/env python3

import sys

import ncs


with ncs.maapi.single_write_trans('admin','delete-oper',
                                  db=ncs.OPERATIONAL) as t:
    r = ncs.maagic.get_root(t)
    del r.myoper[sys.argv[1]]
    t.apply()
