#  The order of packages is significant as there are dependencies between
# the packages. Typically generated namespaces are used by other packages.

ifeq "$(NCS_DIR)" ""
$(error NCS_DIR is not setup. Source ncsrc to setup NSO environment before proceeding)
endif

SINGLE_PACKAGES =  resource-manager \
                   top \
                   top-nano \
                   rfs-vlan \
                   myserv \
                   router \
                   base-config \
                   hostname \
                   intf

CFS_PACKAGES =  resource-manager \
                cfs-top \
		cfs-top-nano

RFS_PACKAGES =  rfs-vlan \
                router

PRE6_PACKAGES =  service-info


# The create-network argument to ncs-netsim
NETWORK = create-device pkg-repo/router-broken ex0 \
          create-device pkg-repo/router ex1 \
          create-device pkg-repo/router ex2 \
          create-device pkg-repo/router ex3 \
          create-device pkg-repo/router ex4 \
          create-device pkg-repo/router ex5 \
          create-device pkg-repo/router ex6 \
          create-device pkg-repo/router-broken ex7

NETSIM_DIR = netsim



NSO_VERSION = $(shell ncs --version)
NSO_VER_MAJ = $(shell echo $(NSO_VERSION) | cut -f1 -d.)
NSO_VER_MIN = $(shell echo $(NSO_VERSION) | cut -f2 -d. | cut -f1 -d_)
NSO_MAJOR_VERSION = $(NSO_VER_MAJ).$(NSO_VER_MIN)

ifeq (, $(shell which python))
 $(error python not found in PATH.)
endif

PYTHON_MAJOR_VERSION=$(shell python --version 2>&1 | cut -f2 -d' ' | cut -f1 -d\.)

ifeq "$(NSO_MAJOR_VERSION)" "5.2"
LSA_NED=tailf-nso-nc-$(NSO_MAJOR_VERSION)
  ifeq "$(PYTHON_MAJOR_VERSION)" "3"
    $(error pyang embedded with NSO version 5.2 requires Python 2.x to work. A simple solution is to patch $(NCS_DIR)/lib/pyang/bin/pyang to use python2 and comment this error message.)
  endif
else ifeq "$(NSO_MAJOR_VERSION)" "5.3"
LSA_NED=tailf-nso-nc-$(NSO_MAJOR_VERSION)
else
LSA_NED=cisco-nso-nc-$(NSO_MAJOR_VERSION)
RFS_NED_OPTIONS=--lsa-lower-nso $(LSA_NED)
endif

# Check if version is pre NSO6
ifeq ($(shell test $(NSO_VER_MAJ) -lt 6; echo $$?),0)
  SINGLE_PACKAGES += ${PRE6_PACKAGES}
  CFS_PACKAGES += ${PRE6_PACKAGES}
  RFS_PACKAGES += ${PRE6_PACKAGES}
endif


.PHONY: all
all:
	@echo "Current build: $(wildcard *-BUILD)"
	@echo "NSO major version: $(NSO_MAJOR_VERSION) ($(NSO_VERSION))"
	@echo
	@echo "Makefile rules:"
	@echo " * single        Setup a single node NSO system."
	@echo " * lsa           Setup an LSA NSO system with one CFS and two RFS nodes."
	@echo " * start         start environment"
	@echo " * stop          stop environment"
	@echo " * bash-<host>   start a bash shell in node/container <host>"
	@echo " * cli-<host>    start an NSO CLI in node/container <host>"

.PHONY: check-build
check-build:
	@if [ ! -e SINGLE-BUILD -a ! -e LSA-BUILD ]; then \
	  echo 'ERROR: You need to build before starting. Run "make single" or "make lsa" to build.'; \
	  exit 1; \
        fi

.PHONY: single
single: SINGLE-BUILD packages ncs.conf $(NETSIM_DIR) venv initial-data
	ln -f netsim-control-panel-single.conf netsim-control-panel.conf

.PHONY: SINGLE-BUILD
SINGLE-BUILD:
	@if [ -e LSA-BUILD ]; then \
	  echo 'ERROR: Already built for LSA. Run "make clean single" to rebuild for single node.'; \
	  exit 1; \
        fi
	@touch SINGLE-BUILD

.PHONY: lsa
lsa: LSA-BUILD upper-nso lower-nso-1 lower-nso-2 $(NETSIM_DIR) venv
	ln -f netsim-control-panel-lsa.conf netsim-control-panel.conf

.PHONY: LSA-BUILD
LSA-BUILD:
	@if [ -e SINGLE-BUILD ]; then \
	  echo 'ERROR: Already built for single node. Run "make clean lsa" to rebuild for lsa.'; \
	  exit 1; \
        fi
	@touch LSA-BUILD

.PHONY: build-pkgs
build-pkgs: pkg-repo/BUILT
pkg-repo/BUILT:
	for i in $(shell find pkg-repo -type d -maxdepth 1 -mindepth 1); do \
	  $(MAKE) -C $${i}/src all || exit 1; \
	done
	touch pkg-repo/BUILT

.PHONY: venv
venv: venv/bin/activate
venv/bin/activate:
	python3 -m venv venv
	(. venv/bin/activate; python3 -m pip install -r requirements.txt)

#
# Single node
#

ncs.conf:
	ncs-setup --dest .

packages: build-pkgs
	mkdir -p packages
	for i in $(SINGLE_PACKAGES); do \
	  ln -sf ../pkg-repo/$${i} packages/.; \
	done

initial-data:
	ncs-netsim ncs-xml-init > ./ncs-cdb/netsim_devices_init.xml
	cp initial_data/resource-pools.xml ncs-cdb/.
	cp initial_data/service-plan-notifications.xml ncs-cdb/.
	cp initial_data/global-settings.xml ncs-cdb/.
#
# LSA
#

upper-nso: build-pkgs
	ncs-setup --no-netsim --dest $@
	if [ -e nso-etc/$@/ncs.conf-$(NSO_MAJOR_VERSION) ]; then \
	cp nso-etc/$@/ncs.conf-$(NSO_MAJOR_VERSION) $@/ncs.conf; \
	else \
	cp nso-etc/$@/ncs.conf $@; \
	fi
	cp initial_data/global-settings.xml $@/ncs-cdb/.
	for i in $(CFS_PACKAGES); do \
	  ln -sf ../../pkg-repo/$${i} $@/packages/.; \
	done
	$(MAKE) $@/packages/rfs-vlan-ned
	ln -s ${NCS_DIR}/packages/lsa/$(LSA_NED) $@/packages/.

upper-nso/packages/rfs-vlan-ned:
	ncs-make-package --no-netsim --no-java --no-python \
	    $(RFS_NED_OPTIONS) \
	    --lsa-netconf-ned pkg-repo/rfs-vlan/src/yang \
	    --dest $@ --build $(@F)

lower-nso-%: build-pkgs
	ncs-setup --no-netsim --dest $@
	cp nso-etc/$@/ncs-cdb/devs.xml $@/ncs-cdb/.
	cp initial_data/global-settings.xml $@/ncs-cdb/.
	if [ -e nso-etc/$@/ncs.conf-$(NSO_MAJOR_VERSION) ]; then \
	cp nso-etc/$@/ncs.conf-$(NSO_MAJOR_VERSION) $@/ncs.conf; \
	else \
	cp nso-etc/$@/ncs.conf $@; \
	fi
	for i in $(RFS_PACKAGES); do \
	  ln -sf ../../pkg-repo/$${i} $@/packages/.; \
	done

#
# netsim
#

.PHONY: $(NETSIM_DIR)
$(NETSIM_DIR): build-pkgs $(NETSIM_DIR)/.netsiminfo
$(NETSIM_DIR)/.netsiminfo:
	ncs-netsim --dir netsim $(NETWORK)



.PHONY: clean
clean:
	rm -rf packages upper-nso lower-nso-1 lower-nso-2
	for i in $(shell find pkg-repo -type d -maxdepth 1 -mindepth 1); do \
	  $(MAKE) -C $${i}/src clean || exit 1; \
	done
	rm -f pkg-repo/BUILT
	rm -rf ncs-cdb state logs scripts target ncs.conf storedstate
	rm -rf netsim README.ncs
	rm -rf __pycache__
	rm -f *.log
	rm -f netsim-control-panel.conf
	rm -f SINGLE-BUILD LSA-BUILD
	@echo
	@echo "NOTE! Directory 'venv' is not removed."
	@echo "      It must be manually deleted to be rebuilt."

.PHONY: start
start: check-build
	ncs-netsim start
	@if [ -e SINGLE-BUILD ]; then \
	  $(MAKE) start-single; \
	fi
	@if [ -e LSA-BUILD ]; then \
	  $(MAKE) start-lsa; \
	fi

.PHONY: start-single
start-single:
	ncs
	./initial_data/startup.sh


.PHONY: start-lsa
start-lsa:
	cd upper-nso;   NCS_IPC_PORT=4569 sname=upper-nso ncs -c ncs.conf
	cd lower-nso-1; NCS_IPC_PORT=4570 sname=lower-nso-1 ncs -c ncs.conf
	cd lower-nso-2; NCS_IPC_PORT=4571 sname=lower-nso-2 ncs -c ncs.conf
	./initial_data/startup-lsa.sh

.PHONY: stop
stop: check-build
	-ncs-netsim stop
	@if [ -e SINGLE-BUILD ]; then \
	  $(MAKE) stop-single; \
	fi
	@if [ -e LSA-BUILD ]; then \
	  $(MAKE) stop-lsa; \
	fi

.PHONY: stop-single
stop-single:
	-ncs --stop

.PHONY: stop-lsa
stop-lsa:
	-NCS_IPC_PORT=4569 ncs --stop
	-NCS_IPC_PORT=4570 ncs --stop
	-NCS_IPC_PORT=4571 ncs --stop

.PHONY: reset
reset:
	ncs-setup --reset

#
# CLI
#

.PHONY: cli cli-upper-nso cli-lower-nso-1 cli-lower-nso-2
cli:
	ncs_cli -u admin
cli-upper-nso: cli
cli-lower-nso-1:
	NCS_IPC_PORT=4570 ncs_cli -C -u admin
cli-lower-nso-2:
	NCS_IPC_PORT=4571 ncs_cli -C -u admin

.PHONY: test
test: test-simple test-rfm test-nano


.PHONY: clean-test
clean-test:
	./cleanup.sh

.PHONY: test-simple
test-simple:
	@if [ -e SINGLE-BUILD ]; then \
	  $(MAKE) test-single-simple; \
	elif [ -e LSA-BUILD ]; then \
	  $(MAKE) test-lsa-simple; \
	fi

.PHONY: test-single-simple
test-single-simple:
	./nso-scenario run -c SIMPLE-SERVICE-CQ-ROLLBACK-ON-ERROR
	./nso-scenario run -c SIMPLE-SERVICE-NOCQ-FAILING-DEVICE
	./nso-scenario run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR
	./nso-scenario run -c 2-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-NON-ATOMIC
	./nso-scenario run -c 3-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR
	./nso-scenario run -c 4-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR-NON-ATOMIC
	./nso-scenario run -c 6-STACKED-SERVICES-CQ-STOP-ON-ERROR-NON-ATOMIC
	./nso-scenario run -c 7-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-RESP
	./nso-scenario run -c 8-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-RESP
	./nso-scenario run -c 9-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-NOT-RESP
	./nso-scenario run -c 10-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-NOT-RESP
	./nso-scenario run -c 11-STACKED-SERVICES-CQ-STOP-ON-ERROR-ROLLBACK-NOT-RESP

.PHONY: test-lsa-simple
test-lsa-simple:
	./nso-scenario -d scenarios-lsa run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR
	./nso-scenario -d scenarios-lsa run -c 2-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-lsa run -c 3-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR
	./nso-scenario -d scenarios-lsa run -c 4-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-lsa run -c 6-STACKED-SERVICES-CQ-STOP-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-lsa run -c 7-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-RESP
	./nso-scenario -d scenarios-lsa run -c 8-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-RESP
	./nso-scenario -d scenarios-lsa run -c 9-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-NOT-RESP
	./nso-scenario -d scenarios-lsa run -c 10-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-NOT-RESP
	./nso-scenario -d scenarios-lsa run -c 11-STACKED-SERVICES-CQ-STOP-ON-ERROR-ROLLBACK-NOT-RESP

.PHONY: test-rfm
test-rfm:
	@if [ -e SINGLE-BUILD ]; then \
	  $(MAKE) test-single-rfm; \
	elif [ -e LSA-BUILD ]; then \
	  $(MAKE) test-lsa-rfm; \
	fi

.PHONY: test-single-rfm
test-single-rfm:
	if [ "$(NSO_MAJOR_VERSION)" != "5.7" ]; then \
	./nso-scenario -d scenarios-rfm run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-pre5.7; \
	else \
	./nso-scenario -d scenarios-rfm run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR; \
	fi
	./nso-scenario -d scenarios-rfm run -c 2-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-rfm run -c 3-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR
	./nso-scenario -d scenarios-rfm run -c 4-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-rfm run -c 6-STACKED-SERVICES-CQ-STOP-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-rfm run -c 7-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-RESP
	./nso-scenario -d scenarios-rfm run -c 8-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-RESP
	./nso-scenario -d scenarios-rfm run -c 9-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-NOT-RESP
	./nso-scenario -d scenarios-rfm run -c 10-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-NOT-RESP
	if [ "$(NSO_MAJOR_VERSION)" == "5.7" ]; then \
	./nso-scenario -d scenarios-rfm run -c 11-STACKED-SERVICES-CQ-STOP-ON-ERROR-ROLLBACK-NOT-RESP; \
	fi

.PHONY: test-lsa-rfm
test-lsa-rfm:
	if [ "$(NSO_MAJOR_VERSION)" != "5.7" ]; then \
	./nso-scenario -d scenarios-lsa-rfm run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-pre5.7; \
	else \
	./nso-scenario -d scenarios-lsa-rfm run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR; \
	fi
	./nso-scenario -d scenarios-lsa-rfm run -c 2-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-lsa-rfm run -c 3-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR
	./nso-scenario -d scenarios-lsa-rfm run -c 4-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-lsa-rfm run -c 6-STACKED-SERVICES-CQ-STOP-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-lsa-rfm run -c 7-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-RESP
	./nso-scenario -d scenarios-lsa-rfm run -c 8-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-RESP
	./nso-scenario -d scenarios-lsa-rfm run -c 9-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-NOT-RESP
	./nso-scenario -d scenarios-lsa-rfm run -c 10-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-NOT-RESP
	if [ "$(NSO_MAJOR_VERSION)" == "5.7" ]; then \
	./nso-scenario -d scenarios-lsa-rfm run -c 11-STACKED-SERVICES-CQ-STOP-ON-ERROR-ROLLBACK-NOT-RESP; \
	fi

.PHONY: test-nano
test-nano:
	@if [ -e SINGLE-BUILD ]; then \
	  $(MAKE) test-single-nano; \
	elif [ -e LSA-BUILD ]; then \
	  $(MAKE) test-lsa-nano; \
	fi

.PHONY: test-single-nano
test-single-nano:
	if [ "$(NSO_MAJOR_VERSION)" != "5.7" ]; then \
	./nso-scenario -d scenarios-nano run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-pre5.7; \
	else \
	./nso-scenario -d scenarios-nano run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR; \
	fi
	./nso-scenario -d scenarios-nano run -c 2-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-nano run -c 3-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR
	./nso-scenario -d scenarios-nano run -c 4-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-nano run -c 6-STACKED-SERVICES-CQ-STOP-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-nano run -c 7-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-RESP
	./nso-scenario -d scenarios-nano run -c 8-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-RESP
	./nso-scenario -d scenarios-nano run -c 9-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-NOT-RESP
	./nso-scenario -d scenarios-nano run -c 10-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-NOT-RESP
	if [ "$(NSO_MAJOR_VERSION)" == "5.7" ]; then \
	./nso-scenario -d scenarios-nano run -c 11-STACKED-SERVICES-CQ-STOP-ON-ERROR-ROLLBACK-NOT-RESP; \
	fi

.PHONY: test-lsa-nano
test-lsa-nano:
	if [ "$(NSO_MAJOR_VERSION)" != "5.7" ]; then \
	./nso-scenario -d scenarios-lsa-nano run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-pre5.7; \
	else \
	./nso-scenario -d scenarios-lsa-nano run -c 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR; \
	fi
	./nso-scenario -d scenarios-lsa-nano run -c 2-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-lsa-nano run -c 3-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR
	./nso-scenario -d scenarios-lsa-nano run -c 4-STACKED-SERVICES-CQ-CONTINUE-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-lsa-nano run -c 6-STACKED-SERVICES-CQ-STOP-ON-ERROR-NON-ATOMIC
	./nso-scenario -d scenarios-lsa-nano run -c 7-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-RESP
	./nso-scenario -d scenarios-lsa-nano run -c 8-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-RESP
	./nso-scenario -d scenarios-lsa-nano run -c 9-STACKED-SERVICES-CQ-STOP-ON-ERROR-UNLOCK-NOT-RESP
	./nso-scenario -d scenarios-lsa-nano run -c 10-STACKED-SERVICES-CQ-STOP-ON-ERROR-DELETE-NOT-RESP
	if [ "$(NSO_MAJOR_VERSION)" == "5.7" ]; then \
	./nso-scenario -d scenarios-lsa-nano run -c 11-STACKED-SERVICES-CQ-STOP-ON-ERROR-ROLLBACK-NOT-RESP; \
	fi

.PHONY: test-commit-queue
test-commit-queue:
	# These scenarios are not updated with any appropriate checks, e.g.
	# verification of output from check-sync and delays or waiting for
	# commit-queues to be in expected state.
	./nso-scenario -d scenarios-commit-queue run -c APPLY-DEVICE-TEMPLATE
	# Race condition when ran with -c
	#./nso-scenario -d scenarios-commit-queue run -c ATOMIC
	./nso-scenario -d scenarios-commit-queue run -c BUNDLING
	./nso-scenario -d scenarios-commit-queue run -c CHK-ROLLBACK-ON-ERROR
	./nso-scenario -d scenarios-commit-queue run -c NO-OVERLAP
	./nso-scenario -d scenarios-commit-queue run -c OVERLAP
	./nso-scenario -d scenarios-commit-queue run -c ROLLBACK-ON-ERROR
	./nso-scenario -d scenarios-commit-queue run -c ROLLBACK-ON-ERROR2
	./nso-scenario -d scenarios-commit-queue run -c ROLLBACK-ON-ERROR3
	./nso-scenario -d scenarios-commit-queue run -c SYNC-ASYNC
	# Race condition when ran with -c
	#./nso-scenario -d scenarios-commit-queue run -c SYNC-ASYNC2

