# NSO Scenario Runner

## Overview

NSO Scenario Runner is a simple tool that uses the same API as the NSO Portal
(nsop.py) to access NSO for queries and to do transactions.

The purpose is to be able to create "scenarios" that produce a consistent output
that can be used as reference for explaining how commit queues work. But also
to be used as a tool for creating your own scenarios to explore the behavior of
your specific services.

The goals of the tool have been:
 * Run repetable scenarios
 * Clear, simple scenario files that are east to interpret
 * Clear output that is easy to interpret

## Help

```
❯ ./nso-scenario -h
usage: nso-scenario [-h] [--user USERNAME] [--pwd PASSWORD] [-p PORT] [-n SERVER] [-d DIR] [--debug] {list,run,help,info} ...

positional arguments:
  {list,run,help,info}

optional arguments:
  -h, --help            show this help message and exit
  --user USERNAME       username (default=admin)
  --pwd PASSWORD        password (default=admin)
  -p PORT, --port PORT  NSO port (default=8080)
  -n SERVER, --nso-host SERVER
                        NSO host (default=localhost)
  -d DIR, --dir DIR     scenario directory (default=scenarios)
  --debug               Enable debug

commands arguments: (-h for details)
  list [-h]                                                  list scenarios
  run [-h] [-c | -s] [-a] [-f FILE] [-P PARAMS] [scenario]   run specified scenario
  help [-h] [-a | command]                                   show information about implemented scenario commands
  info [-h] [scenario]                                       show information about scenarios selected with -d
```

Use option '-h' after a command name to get detailed help for a specific command, e.g:
```
❯ ./nso-scenario run -h
usage: nso-scenario run [-h] [-c | -s] [-a] [-f FILE] [-P PARAMS] [scenario]

Run specified scenario.

positional arguments:
  scenario              scenario to run

optional arguments:
  -h, --help            show this help message and exit
  -c, --no-pause        Run scenario with no pause
  -s, --step            Pause on each step
  -a, --no-abort        Do not abort on failed step
  -f FILE, --file FILE  Run specified scenario file
  -P PARAMS, --params PARAMS
                        Read parameters from json-file.
```

## List available scenarios

```
❯ ./nso-scenario list

```

Use the option '-d' to list scenarios in a different directory.

## Run a scenario

```
❯ ./nso-scenario run 1-STACKED-SERVICES-CQ-ROLLBACK-ON-ERROR
```

Use the option '-d' to list scenarios in a different directory.

