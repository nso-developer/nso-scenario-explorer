# Overview

This collection of scenarions are about to get an understanding how the
commit-queue settings, atomic and error-option, affects the behavior of NSO
and what the resulting state will be in terms of devices sync state and if the
service instance(s) are rolled back or not, for nano services in an LSA
deployment.

## Common scenario criterias

 - devices global-settings commit-queue retry-attemts != unlimited
 - stacked services, with top service using reactive FASTMAP.

## Lessons to learn

NOTE: They are a summary of running these scenarions, but there might be
other cirumstances giving alternate results.

 - A commit-queue id is not always returned from the commit command.
 - The use of commit-queue tag or trace-id is a good way of keeping track of
   the state changes of queue items.
 - For rollback to occur atomic need to be true.
 - rollback-on-error does not work prior to version 5.7.
 - Rollback of the service(s) occur when all retry attemps have been consumed
   and either:
   - error-option is rollback-on-error
   - error-option is stop-on-error and rollback action is called on the locked
     commit-queue item. Rollback need to be executed on both the CFS and RFS
     nodes failed queue-items:s separately.
 - When no rollback has occurred:
   - atomic true -> all devices will be out-of-sync.
   - atomic false -> only failed devices will be out-of-sync.
