# Overview

This collection of scenarions are about to get an understanding how the
commit-queue settings, atomic and error-option, affects the behavior of NSO
and what the resulting state will be in terms of devices sync state and
commit-queue state with retry-attemps set to unlimited.

Which is useful for pushing configuration to a large number of devices and where
a number of them might not be responding and you want to retry until the
configuration is pushed out.

## Common scenario criterias:

 - devices global-settings commit-queue retry-attemts == unlimited
 - simple service

## Lessons to learn

NOTE: They are a summary of running these scenarions, but there might be
other cirumstances giving alternate results.

 - error-option rollback-on-error does not work with retry-attemps set to
   unlimited.
 - NSO has no distinction between a device that is not reachable
   (transient error) or a device rejecting configuration (permanent error).
 - Atomic true will/may result in a rollback transaction to all devices that
   have received configuraion for each retry attempt when at least one device
   fails.
   When pushing configuration a large number of devices it might be wise to use
   atomic false if retry-attemps is unlimited or a large number.
