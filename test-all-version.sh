#!/bin/bash


VERSIONS="5.2 5.3 5.4 5.5 5.6 5.7"

for b in single lsa; do
  for v in $VERSIONS; do
      echo $b $v
      (\
          echo ==========================================================;\
          echo ======== Building and testing $b with NSO $v ========;\
          echo ==========================================================;\
          . ~/ncs-release/$v/ncsrc;\
          make $b start;\
          . venv/bin/activate;\
          make test;\
          make stop clean;\
      ) 2>&1 | tee test-result-$b-$v.txt
  done
done
